<?php
return [
	'template'               => [
		'view_suffix'  => 'htm',
		],
    'view_replace_str'       => [
        '__ADMIN__' => '/static/admin',
        '__IMG__' => ''
    ],
];